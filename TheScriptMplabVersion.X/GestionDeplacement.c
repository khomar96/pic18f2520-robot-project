#include <stdio.h>
#include <stdlib.h>
#include "p18f2520.h"
#include "DebugUART.h"
#include "GestionDeplacement.h"
#include "telecommande.h"
#define RoueDDirecte 0
#define RoueGDirecte 1
#define RoueDIndirecte 1
#define DistanceMinimale 102 // 255*(2(volts)/5(volts)=102
extern char unsigned PresenceObstacle;
extern char unsigned EnMarche;
extern char unsigned start;
extern short unsigned DebutRotation;
extern unsigned short DistanceParcourue; 
extern unsigned char FinRotation;

void StopMoteurs(void){
    EnMarche=0;
    CCPR1L=0;
    CCPR2L=0;
    
}


void MarcheMoteurs(void){
    

    if(PresenceObstacle==0 && DistanceParcourue==0){//Tant qu'on a pas atteint un obstacle/qu'on a pas parcouru un metre
        EnMarche=1;
        CCPR1L=50;//On continue � se d�placer en ligne droite
        CCPR2L=50;
    }
    else
         StopMoteurs();//A la sortie de la boucle soit on a parcourue 1Metre soit on a rencontr� un obstacle
    
    
    
}




void TournerDroite(void){
    StopMoteurs();//On arrete le robot
    
    DebutRotation=1;//On signale le d�but de la rotation

    while(DebutRotation){
        CCPR1L=50;
        
        CCPR2L=50;
        
        PORTAbits.RA7=1;//On fait avancer le moteur gauche
        
        PORTAbits.RA6=0;//On fait reculer le moteur droite 
              }
              
       // printf("Sortie de la boucle while(Fin rotation) \r");

        PORTAbits.RA7=1;//On remet les roues dans leur sens habituels.
        
        PORTAbits.RA6=1;

}


void GestionDeplacement(unsigned char  start,unsigned char DistanceObstacleMoyenne){
    
    if(start){//Si le robot est allum�
        
       // printf("DistanceObstacleMoyenne=%d\r", DistanceObstacleMoyenne);
        if(DistanceObstacleMoyenne >=(unsigned char)DistanceMinimale ){
            
            PresenceObstacle=1;//On signale la pr�sence de l'obstacle � l'aide du flag
            
            printf("\rObstacle trop proche!\r");
            
            TournerDroite();//La fonction TournerDroite va arreter le robot et tourner � 90 autour de lui m�me
            
            PresenceObstacle=0;//Reinitialisation de l'obstacle

      }
      
             
      else
     
          
               MarcheMoteurs();

    
        }
   
        
        else{
           
               StopMoteurs();
            

        }
    
    
    
}

