/* 
 * File:   GestionDeplacement.h
 * Author: OMARKHATIB
 *
 * Created on 23 mai 2018, 15:13
 */

#ifndef GESTIONDEPLACEMENT_H
#define	GESTIONDEPLACEMENT_H


void StopMoteurs(void);
void MarcheMoteurs(void);

#endif	

