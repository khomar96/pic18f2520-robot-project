
#include <stdio.h>
#include <stdlib.h>
#include "MI2C.h"
#include <p18f2520.h>

#include "conversionADC.h"
#include "DebugUART.h"

#include "GestionDeplacement.h"

char unsigned MoyDone;
extern unsigned char MoyUBAT;
extern char unsigned Batt_suffisante;
extern unsigned short UBAT;
 extern unsigned char SommeUbat;
 extern unsigned char MoyIR;
 extern  unsigned char compteurUBAT;
  
 extern unsigned char start; // (d�crit l?�tat du robot 0 si  �tat d?arr�t ,1 si en marche ). 
 extern unsigned char DebutConvUbat ; //(mis � 1 quand la conversion de UBAT d�bute) 
 extern unsigned char DebutConvCapteurs; // (mis � 1 quand la conversion de CaptD et CaptG d�bute) 
 
 extern unsigned char NbrIntUBAT;
extern   unsigned char NbrIntIR;

//void PWM(unsigned char start1,unsigned char distanceD,unsigned char distanceG);

#define  UBATmin 162





void SelectionAdcBatterie(void){
    
  ADCON0bits.CHS0=0;
  ADCON0bits.CHS1=1;
  ADCON0bits.CHS2=0;
  ADCON0bits.CHS3=0;
}
void SelectionAdcIRD(void){
    ADCON0bits.CHS=0000;
}

void SelectionAdcIRG(void){
    ADCON0bits.CHS=0001;
}
/*unsigned char LectureADC_Batt (void){
unsigned short MoyUBAT=0;

 SelectionAdcBatterie();
 ADCON0bits.GO = 1; // demarrage ADC
 while ( ADCON0bits.DONE); // attente de fin conversion
 ADCON0bits.GO = 0; // Arret ADC

 if(compteurUBAT!=8){
    UBAT=UBAT+ADRESH;

    compteurUBAT++;
   // MoyDone=0;
 }
 else{
     MoyUBAT=UBAT>>3;
     printf(" UBATMoyenne=%hu\r ",MoyUBAT);
     compteurUBAT=0;
    // MoyDone=1;
     UBAT=0;
     SurveillanceBatterie(MoyUBAT);
 }
}
void SurveillanceBatterie(unsigned short MoyUBAT){
 if (MoyUBAT>UBATmin) // 160 = 10V tension de seuil
 {
 Batt_suffisante=1;//le flag est a 1 si la batterie est suffisante
 //printf("UBAT moyen vaut :%hu\n\n",UBAT);


 PORTBbits.RB5 = 0;
 //MoyDone=0;
// UBAT=0;//r�initialisation UBAT

 }
 if(MoyUBAT<UBATmin )
 {
  //printf("UBAT moyen vaut :%hu\n\n",MoyUBAT);
     printf("Batterie faible!\r");
 Batt_suffisante=0;//il est � 0 sinon
 PORTBbits.RB5 = ~PORTBbits.RB5;
 //printf("Arret Robot car batterie insuffisante\r ");
     //UBAT=0;//r�initialisation UBAT
//MoyDone=0;
 }
}


*/





void LectureADC_Batt (void){
 SelectionAdcBatterie();
 ADCON0bits.GO = 1; // demarrage ADC
 while ( ADCON0bits.DONE); // attente de fin conversion

 if(compteurUBAT!=8){
    UBAT=UBAT+(signed short)ADRESH;

    compteurUBAT++;
    MoyDone=0;
 }
 else{
     UBAT=UBAT>>3;
     printf(" UBATMoyenne=%hu\r ",UBAT);
     compteurUBAT=0;
     MoyDone=1;
     
     
      if(start){

                Write_PCF8574(0x40,~UBAT);

       }
            else
                Write_PCF8574(0x40,0xFFFF);
     
     
     
 }
 
 if (UBAT>UBATmin && MoyDone==1) // 160 = 10V tension de seuil
 {
 Batt_suffisante=1;//le flag est a 1 si la batterie est suffisante
 //printf("UBAT moyen vaut :%hu\n\n",UBAT);


 PORTBbits.RB5 = 0;
 MoyDone=0;
 UBAT=0;//r�initialisation UBAT

 }
 if(UBAT<UBATmin && MoyDone==1)
 {
  printf("UBAT moyen vaut :%hu\r\r",UBAT);
   
 Batt_suffisante=0;//il est � 0 sinon
 PORTBbits.RB5 = ~PORTBbits.RB5;
   ADCON0bits.GO = 0; // Arret ADC
 printf("\rArret Robot car batterie insuffisante\r ");
     UBAT=0;//r�initialisation UBAT
MoyDone=0;
 }
 ADCON0bits.GO=0;
}
 
 



 
 
 
 void Lire_InfraRouge(void){
      MoyIR=0;

     SelectionAdcIRD();

     ADCON0bits.GO=1;
     while ( ADCON0bits.DONE);
     MoyIR=ADRESH;
          //printf("\rDistance droite =%d\r",MoyIR);

     ADCON0bits.GO=0;

     SelectionAdcIRG();
     ADCON0bits.GO=1;
     while ( ADCON0bits.DONE);
       //  printf("\rDistance gauche =%d\r",ADRESH);

     ADCON0bits.GO=0;
     //MoyIR=ADRESH;
     MoyIR=(MoyIR+ADRESH)>>1;


 
 }
 