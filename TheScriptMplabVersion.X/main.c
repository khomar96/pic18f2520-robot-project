/* 
 * File:   main.c
 * Author: OMARKHATIB
 *
 * Created on 9 avril 2018, 15:30
 */

#include <stdio.h>
#include <stdlib.h>
#include "MI2C.h"
#include <p18f2520.h>
#include <stdbool.h>
#include "GestionDeplacement.h"
#include "conversionADC.h"
#include "telecommande.h"
#include "DebugUART.h"
#pragma config OSC = INTIO67
#pragma config PBADEN  = OFF, WDT = OFF, LVP = OFF, DEBUG = ON//Le portB par d�faut est analogique
//variable globale
char unsigned FinRotation;
char unsigned EnMarche;
char unsigned PresenceObstacle;
char unsigned Batt_suffisante;
volatile char BUFF[5];
char unsigned INT0_TELEC;
unsigned char donnee;
unsigned short UBAT;
unsigned char SommeUbat;
unsigned char MoyIR;
unsigned char distanceD;
unsigned char distanceG;
volatile char TELEC;
unsigned char start; // (d�crit l?�tat du robot 0 si  �tat d?arr�t ,1 si en marche ). 
unsigned char DebutConvUbat ; //(mis � 1 quand la conversion de UBAT d�bute) 
unsigned char DebutConvCapteurs; // (mis � 1 quand la conversion de CaptD et CaptG d�bute)
unsigned char DistanceParcourue; // (mis � 1 quand la conversion de CaptD et CaptG d�bute) 
unsigned char DebutRotation;
unsigned char compteurUBAT;
unsigned short NbrIntTour;
unsigned  NbrIntUBAT;
unsigned char NbrIntIR;
unsigned short long NbrIntDistance;

//void PWM(unsigned char start1,unsigned char distanceD,unsigned char distanceG);

void HighISR(void);//prototype de la routine d'interruption de plus grande priorit�

#pragma code HighVector=0x08//adresse du vecteur d'interruption de haute priorit�
void HighVector(void)
{
    _asm goto HighISR _endasm
    //permettre de se brancher sur la fonction d'interruption
}
#pragma code
#pragma interrupt HighISR //veut dire que la fonction HighISR est une fonction de'interruption



void HighISR(void)//fonction de la routine d'interruption
{
   if(INTCONbits.INT0IF){//Si D�clenchement de l'interruption  de la t�l�commande 
   
      INTCONbits.INT0IF =0;//On acquitte interruptionl
      printf("start=%d\r",start);
      INT0_TELEC=1;
      printf("Bouton ON/OFF appuy�!\r");

                  
           
      }
            
   if(PIR1bits.TMR2IF){//Si d�clenchement de l'interruption Timer 2//
       PIR1bits.TMR2IF=0;
           if(NbrIntUBAT==100){//Pour compter 1 seconde
               DebutConvUbat=1;
                NbrIntUBAT=0;

           }
           
           else
               
               NbrIntUBAT++;
       if(NbrIntDistance==8800){//Pour une distance de 1
               DistanceParcourue=1;
               NbrIntDistance=0;
               printf("On a parcouru 1 metre\r");
       }
       
   
        else{
       if(EnMarche){
            NbrIntDistance++;
            
       }
        if(EnMarche==0)
            NbrIntDistance=0;
            
        }
   if(DebutRotation){//Si le robot est en rotation..           
       if(NbrIntTour==1845){//Pour un angle de 90� degr�s
        //  printf("Fin Rotation\r");
           DebutRotation=0;
           NbrIntTour=0;//r�initialisation
       }
       else{
           NbrIntTour++;
       
       }
           
   }
       
   }
        if(INTCONbits.TMR0IF)	//Si d�clenchement de l'interruption du timer 0		
	{				
            
           TMR0H = 0xB1; //r�armement
           TMR0L = 0xDF;	

           INTCONbits.TMR0IF = 0;//Acquittement
        if(NbrIntIR==10){//Pour 100 ms

           DebutConvCapteurs=1;//mise � 1 du flag

           NbrIntIR=0;//r�initialisation du compteur � 0
	}
        else
           NbrIntIR++;
        
}

      
       
   }
      
void Initialisation(void){
    DebutRotation;
    EnMarche=0;
    NbrIntTour=0;
    PresenceObstacle=0;
    DistanceParcourue=0;
    NbrIntDistance=0;
    NbrIntIR=0;
    NbrIntUBAT=0;
    compteurUBAT=0;
    FinRotation=1;
    start=0;
    UBAT=0;
    DebutConvUbat=0;  //(mis � 1 quand la conversion de UBAT d�bute) 
    DebutConvCapteurs=0;
    INT0_TELEC=0;
    Batt_suffisante=1;
    OSCCONbits.IRCF0 = 1 ;
    OSCCONbits.IRCF1 = 1 ;
    OSCCONbits.IRCF2 = 1 ;//Pour r�gler l'horloge � 8MHZ

    TRISBbits.RB5=0;//led 8 en sortie
    TRISCbits.RC6=1;//Initialistation Tx
    TRISCbits.RC7=0;//Initialisation RX
    TRISCbits.RC5=0;
    TRISCbits.RC1=0;
    TRISCbits.RC2=0;
    TRISAbits.RA6=0;
    TRISAbits.RA7=0;//en sortie
    PORTAbits.RA7=1;
    PORTAbits.RA6=1;
    //Signaux en entr�e
    TRISAbits.RA0=1;
    TRISAbits.RA1=1;//Pour les  capteurs infrarouges
    TRISAbits.RA2=1;//Ubat quantum=60mV
    TRISBbits.RB0=1;//bouton ON/OFF en entr�e


    //section conversion A-N:pour avoir AN0-AN1-AN2 en analogique il faut que 
    //PCFG3:PCFG0=1100 voir data sheet

    /*****/
    //On prend VREF+=VDD et VREF-=VSSc/8)
    //et TACQ=4microsecondes=4*Tad
    //On prend Tad=1micro-secondes (Fos

    ADCON1bits.PCFG3=1;
    ADCON1bits.PCFG2=1;
    ADCON1bits.PCFG1=0;
    ADCON1bits.PCFG0=0;



    ADCON1bits.VCFG0 = 0;
    ADCON1bits.VCFG1 = 0; 
    
    ADCON2bits.ADCS2 = 0; 
    ADCON2bits.ADCS1 = 0;
    ADCON2bits.ADCS0 = 1;


    ADCON2bits.ACQT2 = 0; 
    ADCON2bits.ACQT1 = 1;
    ADCON2bits.ACQT0 = 0;


    //pour s�lectionner  AN2 en Analog Channel



    ADCON2bits.ADFM=0;//Pourun r�sulat sur 8 bits on justifie � gauche
    ADCON0bits.ADON=1;//On valide l'adc
    PIE1bits.ADIE=0;

    /*Initialisation PWM:
    FPWM=1000 hz

    avec un prescaler de 16 on obtient PR2=(16*10^6)/(4*1000*16)-1=124
    * 
    * On choisit un duty cycle de 32% CCPRXL=(32*8*10^6)/(100*8*1000)=80.
    * CCPRXL=80
    * 

    */


    T2CONbits.T2CKPS0 = 0; //Prescaler 16

    T2CONbits.T2OUTPS3 = 1; //Postscaler � 10
    T2CONbits.T2OUTPS2 = 0;
    T2CONbits.T2OUTPS1 = 1;
    T2CONbits.T2OUTPS0 = 0;

    PR2 = 124; //choix p�riode PWM sur 8bits PWM=8000(4*1*16)-1=124
    CCP2CONbits.DC2B1 = 0; //CCP2 = moteur droit
    CCP2CONbits.DC2B0 = 0;
    CCP1CONbits.DC1B1 = 0; //CCP1 = moteur gauche
    CCP1CONbits.DC1B0 = 0;
    CCP2CONbits.CCP2M3 = 1; //Choix du mode PWM pour le moteur droit
    CCP2CONbits.CCP2M2 = 1;
    CCP1CONbits.CCP1M3 = 1;//Choix du mode PWM pour le moteur gauche
    CCP1CONbits.CCP1M2 = 1;
    CCPR1L = 0; // On initialise CCPR1L ET CCPR2L ici , mais sera modifier dans la fonction PWM())
    CCPR2L = 0;

    T2CONbits.TMR2ON = 1; //Validation Timer 2
    PIE1bits.TMR2IE = 1; //Active l'interruption TIMER

    /*Initialisation Timer 0
    * Pour 10 ms:
    avec un prescaler de 1 on aura (2*10^6)/1=2*10^6
    *avec un overflow de 65535 on obtient une valeur de r�armement de :
    2*10^6*10^-3-15625=45535
    * La conversion se fera donc au bout de 10 overflow du timer 0
    * */
    T0CONbits.T08BIT = 0;
    T0CONbits.T0CS = 0;
    T0CONbits.PSA = 1;
    TMR0H = 0xB1; 
    TMR0L = 0xDF;
    T0CONbits.TMR0ON = 1;
    INTCONbits.TMR0IE = 1;    
    INTCONbits.GIE = 1;

    INTCONbits.PEIE = 1;
    TRISBbits.RB5=0;
    TRISBbits.RB1=0;
    PORTBbits.RB1=0;
    /*UART et TEST */
    BAUDCONbits.BRG16 = 0;
    TXSTAbits.BRGH = 1;
    TXSTAbits.SYNC = 0;
    SPBRGH = 0;
    SPBRG = 52;
    RCSTAbits.SPEN = 1;
    TXSTAbits.TXEN = 1;
    }



    
void main(void) {

    Initialisation();
    MI2CInit();//initialiser I2C
    //printf("Initialisation R�ussie\r");
        
        
        
    while(1){
     
        if(DebutConvUbat==1 && DebutConvCapteurs==0) {
            LectureADC_Batt();
           

            DebutConvUbat=0;
        }

        if(DebutConvCapteurs==1)
   {
           Lire_InfraRouge();

           DebutConvCapteurs=0;
   }
   if(Batt_suffisante){
       
           if(INT0_TELEC){
                GestionTelecom();
                INT0_TELEC=0;//reinitialisation flag telecommande

            }
            
           GestionDeplacement(start, MoyIR);


        }
   
   if(Batt_suffisante==0){
         StopMoteurs();}

 }
        // MoyIR=0;
}